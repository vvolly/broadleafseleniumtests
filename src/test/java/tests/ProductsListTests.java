package tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.HeatClinicBasePage;

import static helpers.Configuration.getConfiguration;
import static helpers.Driver.initializeWebDriver;

public class ProductsListTests {
    private WebDriver driver;

    @BeforeTest
    public void setUp(){
        driver = initializeWebDriver();
        driver.get(getConfiguration().getCustomerSiteURL());
    }

    @Test
    public void testingProductsManipulation(){
        new HeatClinicBasePage(driver)
                .getNavigationMenu()
                .goToHotSaucesPage()
                .addProductToCartByProductId("701");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new HeatClinicBasePage(driver)
                .getNavigationMenu()
                .goToHotSaucesPage();

//        new ProductContainer(driver, productId)
//                .getProductName()
//                .getProductPrice()
//                .addToCart();
    }

    @AfterTest
    public void tearDown(){
        driver.quit();
    }
}
