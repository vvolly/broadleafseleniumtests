package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.*;
import testDatas.RegisterTestData;

import static helpers.Configuration.getConfiguration;
import static helpers.Driver.initializeWebDriver;

public class RegisterTests implements RegisterTestData {

    private WebDriver driver;

    @BeforeTest
    public void setUp(){
        driver = initializeWebDriver();
        driver.get(getConfiguration().getCustomerSiteURL());
    }

    @Test
    public void registerNewUser(){
        HeatClinicBasePage heatClinic = new HeatClinicBasePage(driver);
        heatClinic.goToLoginPage()
                .fillEmail(EMAIL)
                .fillFirstName(FIRST_NAME)
                .fillLastName(LAST_NAME)
                .fillPassword(PASSWORD)
                .fillPasswordConfirm(PASSWORD_CONFIRMED)
                .register();

        AccountPage accountPage = heatClinic.openLoggedUserDropdownAndClick(HeatClinicBasePage.ACCOUNT_ACTIONS.MY_PROFILE);
        heatClinic.openLoggedUserDropdown();

        Assert.assertEquals(heatClinic.getLoggedUserProfileLink().isDisplayed(), true);
        Assert.assertEquals(heatClinic.getLoggedUserLogoutLink().isDisplayed(), true);
        Assert.assertEquals(accountPage.getUserEmailAddress(), EMAIL);
        Assert.assertEquals(accountPage.getUserFirstName(), FIRST_NAME);
        Assert.assertEquals(accountPage.getUserLastName(), LAST_NAME);
    }

    @AfterTest
    public void tearDown(){
        driver.quit();
    }
}