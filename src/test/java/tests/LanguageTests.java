package tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.HeatClinicBasePage;
import pages.NavigationMenu;
import testDatas.LanguageTestData;

import static helpers.Configuration.getConfiguration;
import static helpers.Driver.initializeWebDriver;

public class LanguageTests implements LanguageTestData {
    private WebDriver driver;

    @BeforeTest
    public void setUp(){
        driver = initializeWebDriver();
        driver.get(getConfiguration().getCustomerSiteURL());
    }

    @Test
    public void switchLanguageAndCheckIfChanged(){
        HeatClinicBasePage mainPage = new HeatClinicBasePage(driver);
        NavigationMenu navigationMenu = mainPage
                .switchLanguage(HeatClinicBasePage.LANGUAGE_OPTIONS.SPANISH_MEXICO)
                .getNavigationMenu();

        Assert.assertEquals(mainPage.getIconName(), SPANISH_MX);
        Assert.assertEquals(navigationMenu.getHomePageTranslation(), HOMEPAGE_MX);
        Assert.assertEquals(navigationMenu.getHotSaucesTranslation(), HOT_SAUCES_MX);
        Assert.assertEquals(navigationMenu.getMerchandiseTranslation(), MERCHANDISE_MX);
        Assert.assertEquals(navigationMenu.getClearanceTranslation(), CLEARANCE_MX);
    }

    @AfterTest
    public void tearDown(){
        driver.quit();
    }
}
