package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends BasePage{

    @FindBy(id = "customer.emailAddress")
    WebElement customerEmailAddressInput;

    @FindBy(id = "customer.firstName")
    WebElement customerFirstNameInput;

    @FindBy (id = "customer.lastName")
    WebElement customerLastNameInput;

    @FindBy (css = "#registrationForm > div > div > #password")
    WebElement customerPasswordInput;

    @FindBy (css = "#registrationForm > div > div > #passwordConfirm")
    WebElement customerPasswordConfirmInput;

    @FindBy (css = "#registrationForm > button[type='submit']")
    WebElement registerCustomerButton;

    @Override
    public void reinitPageFactorElements() {
        System.out.println("Login page");
        PageFactory.initElements(driver, this);
    }

    public LoginPage(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public LoginPage fillEmail(String customerEmail){
        type(customerEmailAddressInput, customerEmail);
        return this;
    }

    public LoginPage fillFirstName(String customerFirstName){
        type(customerFirstNameInput, customerFirstName);
        return this;
    }

    public LoginPage fillLastName(String customerLastName){
        type(customerLastNameInput, customerLastName);
        return this;
    }

    public LoginPage fillPassword(String customerPassword){
        type(customerPasswordInput, customerPassword);
        return this;
    }

    public LoginPage fillPasswordConfirm(String customerPasswordConfirm){
        type(customerPasswordConfirmInput, customerPasswordConfirm);
        return this;
    }

    public LoginPage register(){
        click(registerCustomerButton);
        return this;
    }


}
