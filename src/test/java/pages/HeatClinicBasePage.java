package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HeatClinicBasePage extends BasePage {

    @FindBy(css = ".navbar-right > li > a[href*=\'/login\']")
    WebElement loginPage;

    @FindBy(css = "a.dropdown-toggle > i.material-icons")
    WebElement loggedUserDropdown;

    @FindBy (css = ".account-actions > li > a[href*='/account']")
    WebElement loggedUserProfileLink;

    @FindBy (css = ".account-actions > li > a[href*='/logout']")
    WebElement loggedUserLogoutLink;

    @FindBy (css = ".dropdown.languages > a.dropdown-toggle > img")
    WebElement languagesDropdown;

    @FindBy (id = "en_US")
    WebElement englishUSDropdownOption;

    @FindBy (id = "en_GB")
    WebElement englishGBDropdownOption;

    @FindBy (id = "es_MX")
    WebElement spanishMXDropdownOption;

    @FindBy (id = "es_ES")
    WebElement spanishESDropdownOption;

    @FindBy (id = "fr_FR")
    WebElement frenchFRDropdownOption;

    public enum ACCOUNT_ACTIONS {
        MY_PROFILE, LOGOUT
    }

    public enum LANGUAGE_OPTIONS {
        ENGLISH_US, ENGLISH_UK, SPANISH_MEXICO, SPANISH_SPAIN, FRANCE
    }

    @Override
    public void reinitPageFactorElements() {
        System.out.println("HeatClinicBase page");
        PageFactory.initElements(driver, this);
    }

    public HeatClinicBasePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public LoginPage goToLoginPage(){
        click(loginPage);
        return new LoginPage(driver);
    }

    public HeatClinicBasePage openLoggedUserDropdown(){
        click(loggedUserDropdown, loggedUserProfileLink); //better click
        return this;
    }

    public <T> T openLoggedUserDropdownAndClick(ACCOUNT_ACTIONS actions){
        openLoggedUserDropdown();
        switch (actions) {
            case MY_PROFILE:
                click(loggedUserProfileLink);
                return (T) new AccountPage(driver);
            case LOGOUT:
                click(loggedUserLogoutLink);
                return (T) this;
            }
        return (T) this;
        }

    public HeatClinicBasePage switchLanguage(LANGUAGE_OPTIONS selectedLanguage){
        click(languagesDropdown, spanishMXDropdownOption);
        switch(selectedLanguage){
            case ENGLISH_US:
                click(englishUSDropdownOption);
                return this;
            case ENGLISH_UK:
                click(englishGBDropdownOption);
                return this;
            case SPANISH_MEXICO:
                click(spanishMXDropdownOption);
                return this;
            case SPANISH_SPAIN:
                click(spanishESDropdownOption);
                return this;
            case FRANCE:
                click(frenchFRDropdownOption);
                return this;
        }
        return this;
    }

    public NavigationMenu getNavigationMenu(){
        return new NavigationMenu(driver);
    }

    public CartPage getCart(){
        return new CartPage(driver);
    }

    public WebElement getLoggedUserProfileLink(){
        return loggedUserProfileLink;
    }

    public WebElement getLoggedUserLogoutLink(){
        return loggedUserLogoutLink;
    }

    public String getIconName(){
        return languagesDropdown.getAttribute("alt");
    }
}
