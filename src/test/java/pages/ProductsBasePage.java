package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProductsBasePage extends BasePage{

    public ProductsBasePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @Override
    public void reinitPageFactorElements() {
        PageFactory.initElements(driver, this);
    }

    public List<WebElement> getAllProducts(){
        List<WebElement>  productsPageList = new ArrayList<WebElement>();
        productsPageList.addAll(driver.findElement(By.className("col-md-9")).findElements(By.className("col-md-4")));
        return productsPageList;
    }

    public Map<String, WebElement> getMappedProducts(){
        Map<String, WebElement> webElemeentBtProductIdMap = new HashMap<>();
        List<WebElement> productsList = new ArrayList<WebElement>();
        productsList = getAllProducts();
        for(int i = 0; i < productsList.size() -1 ; i++){
            webElemeentBtProductIdMap.put(productsList.get(i).findElement(By.className("js-productContainer")).getAttribute("data-id"), productsList.get(i));
        }
        return webElemeentBtProductIdMap;
    }

    public WebElement getProductByProductId(String productId){
        return getMappedProducts().get(productId);
    }

    public String getProductPriceByProductId(String productId){
        return getProductByProductId(productId).findElement(By.className("price")).getText();
    }

    public String getProductNameByProductId(String productId){
        return getProductByProductId(productId).findElement(By.className("card-title product-title")).getText();
    }

    public ProductsBasePage addProductToWhishlistBtProductId(String productId){
        click(getProductByProductId(productId).findElement(By.className("material-icons")));
        return this;
    }

    public ProductsBasePage addProductToCartByProductId(String productId){
        moveToElementAndClick(getProductByProductId(productId).findElement(By.cssSelector(".js-addToCart")));
        return this;
    }

    public ProductsBasePage showProductQuickViewByProductId(String productId){
        moveToElementAndClick(getProductByProductId(productId).findElement(By.cssSelector(".btn-quickview")));
        return this;
    }

}
