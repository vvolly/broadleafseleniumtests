package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends BasePage{
    public HomePage(WebDriver driver) {
        super(driver);
    }


    @Override
    public void reinitPageFactorElements() {
        System.out.println("Account page");
        PageFactory.initElements(driver, this);
    }
}
