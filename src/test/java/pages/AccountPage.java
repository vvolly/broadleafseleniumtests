package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AccountPage extends BasePage {

    @FindBy (id = "emailAddress")
    WebElement userEmailAddressInput;

    @FindBy (id = "firstName")
    WebElement userFirstNameInput;

    @FindBy (id = "lastName")
    WebElement userLastNameInput;

    public AccountPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @Override
    public void reinitPageFactorElements() {
        System.out.println("Account page");
        PageFactory.initElements(driver, this);
    }

    public String getUserEmailAddress(){
        return userEmailAddressInput.getAttribute("value");
    }

    public String getUserFirstName(){
        return userFirstNameInput.getAttribute("value");
    }

    public String getUserLastName(){
        return userLastNameInput.getAttribute("value");
    }
}
