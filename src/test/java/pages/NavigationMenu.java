package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NavigationMenu extends  BasePage{

    @FindBy(css="#left-nav > ul.main-menu > li > a[href*=\'/\'] > span")
    WebElement homePageLink;

    @FindBy(css="#left-nav > ul.main-menu > li > a[href*=\'/hot-sauces\'] > span")
    WebElement hotSaucesLink;

    @FindBy(css="#left-nav > ul.main-menu > li.dropdown > a[href*=\'/merchandise\'] > span")
    WebElement merchandiseLink;

    @FindBy(css="#left-nav > ul.main-menu > li > a[href*=\'/clearance\'] > span")
    WebElement clearanceLink;


    @Override
    public void reinitPageFactorElements() {
        System.out.println("Navi page");
        PageFactory.initElements(driver, this);
    }

    public NavigationMenu(WebDriver driver){
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public HomePage goToHomePage(){
        homePageLink.click();
        return new HomePage(driver);
    }

    public HotSaucesPage goToHotSaucesPage(){
        hotSaucesLink.click();
        return new HotSaucesPage(driver);
    }

    public String getHomePageTranslation(){
        return getText(homePageLink);
    }

    public String getHotSaucesTranslation(){
        return getText(hotSaucesLink);
    }

    public String getMerchandiseTranslation(){
        return getText(merchandiseLink);
    }

    public String getClearanceTranslation(){
        return getText(clearanceLink);
    }

}
