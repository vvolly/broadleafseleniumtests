package pages;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

public abstract class BasePage {
    protected WebDriver driver;
    protected WebDriverWait wait;
    protected Actions action;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
        this.action = new Actions(driver);
    }

    public void click(WebElement element){
        try{
            waitForElementToBeClickable(element).click();
        }catch (StaleElementReferenceException sere){
            reinitPageFactorElements();
            waitForElementToBeClickable(element).click();
        } catch (TimeoutException te){
            System.out.println("Timeout exception " + element + " was not clicked");
        }
    }

    public void moveToElementAndClick(WebElement elementToBeClicked){
        action.moveToElement(elementToBeClicked).perform();
        click(elementToBeClicked);
    }

    public void click(WebElement elementToBeClicked, WebElement elementToBeDisplayed){
        click(elementToBeClicked);
        waitForElementToBeVisible(elementToBeDisplayed); //add what if its not displayed
    }

    public void type(WebElement element, String text){
        waitForElementToBeVisible(element).clear();
        element.sendKeys(text);
    }

    public String getText(WebElement element){
        return waitForElementToBeVisible(element).getText();
    }

    public WebElement waitForElementToBeClickable(WebElement element){
        new FluentWait<WebDriver>(driver)
                .withTimeout(20, TimeUnit.SECONDS)
                .pollingEvery(10, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .until(ExpectedConditions.elementToBeClickable(element));
        return element;
    }

    public WebElement waitForElementToBeVisible(WebElement element){
        new FluentWait<WebDriver>(driver)
                .withTimeout(20, TimeUnit.SECONDS)
                .pollingEvery(10, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .until(ExpectedConditions.visibilityOf(element));
        return element;
    }

    public abstract void reinitPageFactorElements();
    //public abstract String getUrl();

}
