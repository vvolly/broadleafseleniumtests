package pages;

import org.openqa.selenium.WebDriver;

public class HotSaucesPage extends ProductsBasePage{

    public HotSaucesPage(WebDriver driver) {
        super(driver);
    }

    public HotSaucesPage setManufacturer(){
        return this;
    }

    public HotSaucesPage setHeatRange(){
        return this;
    }

    public HotSaucesPage setPrice(){
        return this;
    }
}
