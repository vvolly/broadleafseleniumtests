package testDatas;

public interface RegisterTestData {
    String EMAIL = "fakemdsaaailinio@gmail.com";
    String FIRST_NAME = "Andrew";
    String LAST_NAME = "Golota";
    String PASSWORD = "fakepassword";
    String PASSWORD_CONFIRMED = "fakepassword";

}
