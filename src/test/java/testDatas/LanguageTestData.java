package testDatas;

public interface LanguageTestData {
    String SPANISH_MX = "Mexico";
    String HOMEPAGE_MX = "INICIO";
    String HOT_SAUCES_MX = "SALSAS";
    String MERCHANDISE_MX = "MERCANCÍA";
    String CLEARANCE_MX = "DESCUENTO";
}
