package helpers;

import java.io.IOException;
import java.util.Properties;

public class Configuration {
    private static final String SITE_CUSTOMER_ADDRESS_PROPERTY = "test.site.url.customer";
    private static final String SITE_ADMIN_ADDRESS_PROPERTY = "test.site.url.admin";
    private static final String DRIVER_LOCATION_PROPERTY = "driver.location";
    private static Configuration configuration;
    private Properties properties;
    private String customerSiteURL;
    private String adminSiteURL;
    private String driverLocation;

    private Configuration(){
        properties = new Properties();
        try {
            properties.load(Configuration.class.getClassLoader().getResourceAsStream("configuration.properties"));
        } catch (IOException exception) {
            throw new ExceptionInInitializerError(exception);
        }
        adminSiteURL = extractProperty(SITE_ADMIN_ADDRESS_PROPERTY);
        customerSiteURL = extractProperty(SITE_CUSTOMER_ADDRESS_PROPERTY);
        driverLocation = extractProperty(DRIVER_LOCATION_PROPERTY);
    }

    private String extractProperty(String propertyName){
        String property = System.getProperty(propertyName);
        if(property == null){
            property = properties.getProperty(propertyName);
        }
        return property;
    }

    public static Configuration getConfiguration(){
        if(configuration == null){
            return new Configuration();
        }
        return configuration;
    }

    public String getAdminSiteURL(){
        return adminSiteURL;
    }

    public String getCustomerSiteURL(){
        return customerSiteURL;
    }


    public String getDriverLocation(){
        return driverLocation;
    }
}
